#include <iostream>
#include <conio.h>
#include <list>
#include <stdlib.h>
using namespace std;

class Node {
public:
		string	data;
		Node*	next;
};
class List{
	public:
			List(void)	{head = NULL;	}
			~List(void);
			
			bool isEmpty(){ return head == NULL;	}
			Node* InsertNode(int index,string x);
			int FindNode(string x);
			int DeleteNode(int index);
			void DisplayList(void);
			void DisplayPlaylist(void);
			string ViewMusic(int index);
	private:
			Node* head;
};
int List::FindNode(string x){
	Node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrNode && CurrNode->data != x){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrIndex;
	return 0;
}
int List:: DeleteNode(int index){
	Node* prevNode	=	NULL;
	Node* CurrNode	=	head;
	int CurrIndex	=	1;
	while (CurrIndex != index){
		prevNode	=	CurrNode;
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode){
		if(prevNode){
			prevNode->next	=	CurrNode->next;
			delete CurrNode;
		}
		else{
			head	=	CurrNode->next;
			delete	CurrNode;
		}
		return	CurrIndex;
	}
	return 0;
}
string List::ViewMusic(int index){
	Node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrIndex != index){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrNode->data;
	return NULL;
}
void List::DisplayPlaylist(){
	int num = 0;
	Node* CurrNode	=	head;
	while (CurrNode != NULL){
		cout<<CurrNode->data<<endl;
		CurrNode	=	CurrNode->next;
		num++;
	}
	
}
void List::DisplayList(){
	int num = 0;
	Node* CurrNode	=	head;
	while (CurrNode != NULL){
		cout<<"["<<num+1<<"]"<<CurrNode->data<<endl;
		CurrNode	=	CurrNode->next;
		num++;
	}
	
}
Node* List::InsertNode(int index,string x){
	if (index < 0) return NULL;
	int CurrIndex = 1;
	Node* CurrNode =head;
	while (CurrNode && index > CurrIndex){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	Node* newNode =		new		Node;
	newNode->data = 	x;
	if (index==0){
			newNode->next	=	head;
			head 			=	newNode;
	}
	else{
		newNode->next	=	CurrNode->next;
		CurrNode->next	=	newNode;
	}
	return newNode;
}
List::~List(void){
	Node* CurrNode	=	head,	*nextNode	=	NULL;
	while (CurrNode != NULL){
		nextNode	=	CurrNode->next;
		delete	CurrNode;
		CurrNode	=	nextNode;
	}
}
int main(){
	string mus,num;
	List MusicList;
	MusicList.InsertNode(0,"Prinsesa - 6CycleMind");
	MusicList.InsertNode(1,"Magbalik - Callalily");
	MusicList.InsertNode(2,"Ligaya - Eraserheads");
	MusicList.InsertNode(3,"Magkabilang Mundo- Jireh Lim");
	MusicList.InsertNode(4,"Martyr Nyebera - Kamikazee");
	MusicList.InsertNode(5,"Migraine - Moonstar88");
	MusicList.InsertNode(6,"One and Only You - Parokya ni Edgar");
	MusicList.InsertNode(7,"Sa Piling Mo - Silent Sanctuary");
	MusicList.InsertNode(8,"Sila - Sud");
	again:
	cout<<"Welcome to your Playlist\nType 1 to Add a new Music title\nType 2 to View/Play a Music title\nType 3 to Edit a Music title\nType 4 to Delete a Music title\nType 5 to view Playlist\nType 6 to Exit\nEnter the chosen Number : ";
	cin>>num;
	if (num=="1"){
		system("CLS");
		string mus;
		cout<<"Please Enter the Title of the music : "<<endl;
		cin.ignore();
		getline(cin,mus);
		MusicList.InsertNode(0,mus);
		goto again;
	}
	else if(num=="2"){
		system("CLS");
		int musnum;
		cout<<"Choose the number of the music you want to play : "<<endl;
		MusicList.DisplayList();
		cin>>musnum;
		cout<<"Now Playing : "<<MusicList.ViewMusic(musnum)<<endl;
		cout<<"Next : "<<MusicList.ViewMusic(musnum+1)<<endl;
		cout<<"Previous : "<<MusicList.ViewMusic(musnum-1)<<endl;	
	}
	else if(num=="3"){
		system("CLS");
	int editnum;
	string editname;
	cout<<"Choose the number of the music title you want to edit : "<<endl;
	MusicList.DisplayList();
	cin>>editnum;
	MusicList.DeleteNode(editnum);
	cout<<"Enter the new music title : ";
	cin.ignore();
	getline(cin,editname);
	cout<<endl;
	MusicList.InsertNode(editnum-1,editname);
	goto again;
	}
	else if(num=="4"){
		system("CLS");
	int del;
	cout<<"Choose the number of the music title you want to delete : "<<endl;
	MusicList.DisplayList();
	cin>>del;
	MusicList.DeleteNode(del);
	goto again;
	}
	else if(num=="5"){
		system("CLS");
		MusicList.DisplayPlaylist();
		cout<<endl;
		goto again;
	}
	else if(num=="6"){
		return 0;
	}
	else{
		cout<<"Enter a number based on the instructions\nPlease Try Again\n\n<---------------------------------------------------------------------->\n\n"<<endl;
		goto again;
	}
}